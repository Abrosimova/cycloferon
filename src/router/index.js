import Vue from 'vue';
import Router from 'vue-router';
import Main from '@/components/Main';
import ArticlesList from '@/components/ArticlesList';
import SingleArticle from '@/components/Article';
import Week from '@/components/Week';
import Quiz from '@/components/Quiz';
import QuizItem from '@/components/QuizItem';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '*',
      name: 'Main',
      component: Main,
    },
    {
      path: '/library/:state',
      name: 'Lib',
      component: ArticlesList,
    },
    {
      path: '/article/:id',
      name: 'Article',
      component: SingleArticle,
    },
    {
      path: '/healthy-week',
      name: 'Week',
      component: Week,
    },
    {
      path: '/quiz',
      name: 'Quiz',
      component: Quiz,
    },
    {
      path: '/quiz/:id',
      name: 'QuizItem',
      component: QuizItem,
    },
  ],
});
