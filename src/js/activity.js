import appOptions from './settings';
import API from './api';
import Rating from './rating';

export default {
  get(user, callback) {
    const that = this;
    that.userId = user.id_str;
    API.getKeyFromDB({ key: `${appOptions.provider}-activity-user_${user.id_str}` }, (data) => {
//      data = null;
      if (data) {
        that.model = JSON.parse(data.Value);
        if (that.model.invite === undefined) {
          that.model.invite = [];
        }
        that.checkDate();
        if (typeof callback === 'function') {
          callback(that.model);
        }
      } else {
        that.model = {
          user,
          last: new Date().getTime(),
          week: [],
          quiz: [],
          articles: [],
          rating: 0,
          invite: [],
        };
        that.set();
        if (typeof callback === 'function') {
          callback(that.model);
        }
      }
    });
  },
  checkDate() {
    const last = new Date(this.model.last).setHours(0, 0, 0, 0);
    const today = new Date().setHours(0, 0, 0, 0);
    if (today > last) {
      this.model.last = new Date().getTime();
      this.model.rating += 10;
      this.set();
    }
  },
  set(callback) {
    const that = this;
    Rating.get((data) => {
      if (data.length < 20 && !data.find(item => item.user.id === this.userId)) {
        data.push({
          user: {
            id: that.model.user.id_str,
            firstName: that.model.user.firstName,
            lastName: that.model.user.lastName,
            photo: that.model.user.photoMedium,
          },
          rating: that.model.rating });
        Rating.set(data);
        return;
      }
      for (let i = 0; i < data.length; i += 1) {
        if (data[i].rating < that.model.rating) {
          data.splice(i, 1, {
            user: {
              id: that.model.user.id_str,
              firstName: that.model.user.firstName,
              lastName: that.model.user.lastName,
              photo: that.model.user.photoMedium,
            },
            rating: that.model.rating,
          });
          Rating.set(data);
          return;
        }
//        data.splice(i, 1, {
//          user: {
//            id: data[i].user.id,
//            firstName: data[i].user.firstName,
//            lastName: data[i].user.lastName,
//            photo: data[i].user.photo,
//          },
//          rating: data[i].rating,
//        });
      }
    });
    API.addKeyToDB({
      label: `${appOptions.provider}-activity`,
      key: `${appOptions.provider}-activity-user_${this.userId}`,
      value: JSON.stringify(that.model),
    }, () => {
      if (typeof callback === 'function') {
        callback();
      }
    });
  },
};
