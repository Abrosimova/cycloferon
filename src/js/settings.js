let isMobile;

if (navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i)
|| navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i)
|| navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i)
|| navigator.userAgent.match(/Windows Phone/i)) {
  isMobile = true;
} else {
  isMobile = false;
}

const locationUrl = document.location.href;

export default (locationUrl.indexOf('school.mosreg') > -1) ?
{
  authUrl: 'https://login.school.mosreg.ru/oauth2',
  grantUrl: 'https://api.school.mosreg.ru/v1/authorizations',
  scope: '',
  clientId: '',
  redirectUrl: `${window.location.href}/?auth=true`,
  provider: 'mosreg-cycloferon',
  api: 'https://api.school.mosreg.ru/v1/',
  isMobile,
  userLink: 'https://school.mosreg.ru/user/user.aspx?user=',
  cdnPath: 'https://ad.csdnevnik.ru/special/staging/cycloferon/img/',
  cdnArticles: 'https://ad.csdnevnik.ru/special/staging/cycloferon/articles/',
  cdnQuiz: 'https://ad.csdnevnik.ru/special/staging/cycloferon/quiz/',
  adminIds: [],
}
:
{
  authUrl: 'https://login.dnevnik.ru/oauth2',
  grantUrl: 'https://api.dnevnik.ru/v1/authorizations',
  scope: 'CommonInfo,FriendsAndRelatives,Messages',
  clientId: 'a13fa6ed57ad4c80a9173baedd1a8528',
  redirectUrl: `${window.location.href}/?auth=true`,
  provider: 'cycloferon',
  api: 'https://api.dnevnik.ru/v1/',
  isMobile,
  userLink: 'https://dnevnik.ru/user/user.aspx?user=',
  cdnPath: 'https://ad.csdnevnik.ru/special/staging/cycloferon/img/',
  cdnArticles: 'https://ad.csdnevnik.ru/special/staging/cycloferon/articles/',
  cdnQuiz: 'https://ad.csdnevnik.ru/special/staging/cycloferon/quiz/',
  adminIds: ['1000006315838', '1000004934922'],
};
