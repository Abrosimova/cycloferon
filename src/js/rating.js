import appOptions from './settings';
import API from './api';

export default {
  get(callback) {
    const that = this;
    API.getKeyFromDB({ key: `${appOptions.provider}-user_rating` }, (data) => {
//      data = null;
      if (data) {
        that.model = JSON.parse(data.Value);
        if (typeof callback === 'function') {
          callback(that.model);
        }
      } else {
        that.model = [];
        that.set();
        if (typeof callback === 'function') {
          callback(that.model);
        }
      }
    });
  },
  set(model = this.model, callback) {
    API.addKeyToDB({
      label: `${appOptions.provider}-rating`,
      key: `${appOptions.provider}-user_rating`,
      value: JSON.stringify(model),
    }, () => {
      if (typeof callback === 'function') {
        callback();
      }
    });
  },
};
