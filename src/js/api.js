﻿import Vue from 'vue';
import VueResource from 'vue-resource';
import Cookie from './cookie';
import Auth from './auth';
import appOptions from './settings';

Vue.use(VueResource);
Vue.http.options.root = appOptions.api;

export default {
  init(token) {
    this.token = token;
    this.model = {};
  },
  getUserInfo(callback) {
    Vue.http.get(`users/me/?access_token=${this.token}`)
      .then((response) => {
        if (undefined !== response) {
          if (typeof callback === 'function') {
            callback(response.body);
          }
        }
      },
      () => {
        Cookie.delete(`${appOptions.provider}_token`);
        Auth.auth();
      });
  },
  addKeyToDB(options = {}, callback) {
    Vue.http.post(`storage/keys/?access_token=${this.token}`,
      {
        label: options.label,
        key: options.key,
        value: options.value,
        permissionLevel: 'Public',
      })
      .then((response) => {
        if (undefined !== response) {
          if (typeof callback === 'function') {
            callback(response.body);
          }
        }
      },
      () => {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
  },
  deleteKeyFromDB(key = '', callback) {
    Vue.http.post(`storage/keys/${key}/delete/?access_token=${this.token}`)
      .then((response) => {
        if (undefined !== response) {
          if (typeof callback === 'function') {
            callback(response.body);
          }
        }
      },
      () => {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
  },
  getKeyFromDB(options = {}, callback) {
    Vue.http.get(`storage/keys/${options.key}/?access_token=${this.token}`)
      .then((response) => {
        if (undefined !== response) {
          if (typeof callback === 'function') {
            callback(response.body);
          }
        }
      },
      () => {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
  },
  getKeysFromDB(options = {}, callback) {
    Vue.http.get(`storage/keys/?label=${options.label}&page_number=${options.pageNumber}&page_size=${options.pageSize}&order_by=date_desc&access_token=${this.token}`)
      .then((response) => {
        if (undefined !== response) {
          if (typeof callback === 'function') {
            callback(response.body);
          }
        }
      },
      () => {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
  },
  getFriends(callback) {
    this.getFriendsIds(() => {
      this.getFriendsProfiles((data) => {
        if (typeof callback === 'function') {
          callback(data);
        }
      });
    });
  },
  getFriendsIds(callback) {
    Vue.http.get(`users/me/friends/?access_token=${this.token}`)
      .then((response) => {
        if (undefined !== response) {
          this.model.friendsId = response.body;
          if (typeof callback === 'function') {
            callback(response.body);
          }
        }
      },
      () => {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
  },
  getFriendsProfiles(callback) {
    Vue.http.post(`users/many/?access_token=${this.token}`, JSON.stringify(this.model.friendsId))
      .then((response) => {
        if (undefined !== response) {
          this.model.friendsProfiles = response.body;
          if (typeof callback === 'function') {
            callback(response.body);
          }
        }
      },
      () => {
        if (typeof callback === 'function') {
          callback(null);
        }
      });
  },
  sendMessage(options = {}, callback) {
    Vue.http.post(`messages/?access_token=${this.token}`,
    JSON.stringify(
      {
        from_str: options.from,
        from: options.from_str,
        to: options.to,
        to_str: options.to_str,
        body: options.body,
      }))
    .then((response) => {
      if (undefined !== response) {
        if (typeof callback === 'function') {
          callback(response.body);
        }
      }
    },
    () => {
      if (typeof callback === 'function') {
        callback(null);
      }
    });
  },
};
