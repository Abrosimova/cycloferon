// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import '@/assets/css/reset.css';
import Vue from 'vue';
import VueResource from 'vue-resource';
import VueAnalytics from 'vue-analytics';
import App from './App';
import router from './router';

Vue.config.productionTip = false;

Vue.use(VueResource);
Vue.use(VueAnalytics, {
  id: 'UA-85680372-32',
  checkDuplicatedScript: true,
});
/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App },
});
